import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split, cross_val_score, GridSearchCV
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.impute import SimpleImputer
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix
import warnings

warnings.filterwarnings('ignore')


class TitanicPredictor:
    def __init__(self):
        self.data = None
        self.X_train = None
        self.X_test = None
        self.y_train = None
        self.y_test = None
        self.model = None
        self.scaler = StandardScaler()

    def load_data(self):
        titanic = sns.load_dataset('titanic')
        self.data = titanic
        print("Dataset loaded successfully. Shape:", self.data.shape)
        return self.data

    def clean_data(self):
        self.data = self.data.drop(['deck', 'embark_town', 'alive'], axis=1)

        self.data['age'].fillna(self.data['age'].median(), inplace=True)
        self.data['embarked'].fillna(self.data['embarked'].mode()[0], inplace=True)

        le = LabelEncoder()
        self.data['sex'] = le.fit_transform(self.data['sex'])
        self.data['embarked'] = le.fit_transform(self.data['embarked'])

        print("Data cleaning completed.")
        return self.data

    def perform_eda(self):
        plt.figure(figsize=(15, 10))

        plt.subplot(2, 2, 1)
        sns.countplot(data=self.data, x='survived')
        plt.title('Survival Distribution')

        plt.subplot(2, 2, 2)
        sns.histplot(data=self.data, x='age', bins=30)
        plt.title('Age Distribution')

        plt.subplot(2, 2, 3)
        sns.countplot(data=self.data, x='class', hue='survived')
        plt.title('Survival by Class')

        plt.subplot(2, 2, 4)
        sns.countplot(data=self.data, x='sex', hue='survived')
        plt.title('Survival by Sex')

        plt.tight_layout()
        return plt

    def engineer_features(self):
        self.data['family_size'] = self.data['sibsp'] + self.data['parch'] + 1

        self.data['is_alone'] = (self.data['family_size'] == 1).astype(int)

        self.data['fare_per_person'] = self.data['fare'] / self.data['family_size']

        print("Feature engineering completed.")
        return self.data

    def prepare_features(self):
        features = ['pclass', 'sex', 'age', 'sibsp', 'parch', 'fare', 'embarked',
                    'family_size', 'is_alone', 'fare_per_person']

        X = self.data[features]
        y = self.data['survived']

        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(
            X, y, test_size=0.2, random_state=42
        )

        self.X_train = self.scaler.fit_transform(self.X_train)
        self.X_test = self.scaler.transform(self.X_test)

        print("Features prepared for modeling.")
        return self.X_train, self.X_test, self.y_train, self.y_test

    def train_model(self, model_type='rf'):
        if model_type == 'rf':
            self.model = RandomForestClassifier(random_state=42)
            param_grid = {
                'n_estimators': [100, 200],
                'max_depth': [10, 20],
                'min_samples_split': [2, 5]
            }
        else:
            self.model = LogisticRegression(random_state=42)
            param_grid = {
                'C': [0.1, 1, 10],
                'solver': ['liblinear', 'lbfgs']
            }

        grid_search = GridSearchCV(self.model, param_grid, cv=5)
        grid_search.fit(self.X_train, self.y_train)

        self.model = grid_search.best_estimator_
        print(f"Best parameters: {grid_search.best_params_}")
        return self.model

    def evaluate_model(self):
        y_pred = self.model.predict(self.X_test)

        print("\nClassification Report:")
        print(classification_report(self.y_test, y_pred))

        cm = confusion_matrix(self.y_test, y_pred)
        plt.figure(figsize=(8, 6))
        sns.heatmap(cm, annot=True, fmt='d', cmap='Blues')
        plt.title('Confusion Matrix')
        plt.ylabel('True Label')
        plt.xlabel('Predicted Label')

        return plt

    def run_pipeline(self):
        self.load_data()
        self.clean_data()
        self.engineer_features()
        self.prepare_features()
        self.train_model()
        return self.evaluate_model()


# Example usage
if __name__ == "__main__":
    predictor = TitanicPredictor()
    predictor.run_pipeline()

predictor = TitanicPredictor()
predictor.run_pipeline()