import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report
from sklearn.datasets import load_iris

iris = load_iris()
df = pd.DataFrame(data=iris.data, columns=iris.feature_names)
df['species'] = pd.Categorical.from_codes(iris.target, iris.target_names)

print("Форма даних:", df.shape)
print("Типи даних:\n", df.dtypes)
print("Перші 3 рядки:\n", df.head(3))

print("\nКлючі даних:", df.keys())
print("Кількість рядків:", df.shape[0])
print("Кількість стовпців:", df.shape[1])
print("Назви ознак:", df.columns.tolist())
print("\nОпис даних:\n", df.describe())

print("\nСпостереження для кожного виду:")
for species in iris.target_names:
    print(f"{species}: {len(df[df['species'] == species])}")

sns.pairplot(df, hue='species')
plt.suptitle('Параметри Ірису')
plt.show()

plt.figure(figsize=(10,6))
sns.countplot(x='species', data=df)
plt.title('Частота видів Ірису')
plt.xticks(rotation=45)
plt.show()

X = df.iloc[:, :-1]
y = df['species']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
print("\nТренувальний набір:", X_train.shape, "Тестовий набір:", X_test.shape)

label_encoder = LabelEncoder()
y_train = label_encoder.fit_transform(y_train)
y_test = label_encoder.transform(y_test)

knn = KNeighborsClassifier(n_neighbors=5)
knn.fit(X_train, y_train)
y_pred = knn.predict(X_test)

target_names = label_encoder.inverse_transform([0, 1, 2])
print("\nЗвіт про класифікацію:\n", classification_report(y_test, y_pred, target_names=target_names))