import seaborn as sns
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split, cross_val_score, GridSearchCV
from sklearn.preprocessing import StandardScaler, OneHotEncoder
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestClassifier
from sklearn.impute import SimpleImputer
from sklearn.metrics import classification_report, confusion_matrix
import matplotlib.pyplot as plt

penguins = sns.load_dataset('penguins')
print("Форма даних:", penguins.shape)
print(penguins.head())

penguins_cleaned = penguins.dropna()

X = penguins_cleaned.drop('species', axis=1)
y = penguins_cleaned['species']

numeric_features = ['bill_length_mm', 'bill_depth_mm', 'flipper_length_mm', 'body_mass_g']
categorical_features = ['island', 'sex']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

numeric_transformer = Pipeline(steps=[
    ('imputer', SimpleImputer(strategy='median')),
    ('scaler', StandardScaler())
])

categorical_transformer = Pipeline(steps=[
    ('imputer', SimpleImputer(strategy='constant', fill_value='missing')),
    ('onehot', OneHotEncoder(handle_unknown='ignore'))
])

preprocessor = ColumnTransformer(
    transformers=[
        ('num', numeric_transformer, numeric_features),
        ('cat', categorical_transformer, categorical_features)
    ])

clf = Pipeline(steps=[('preprocessor', preprocessor),
                      ('classifier', RandomForestClassifier(random_state=42))])

cv_scores = cross_val_score(clf, X_train, y_train, cv=5)
print("Середня точність крос-валідації:", cv_scores.mean())

param_grid = {
    'classifier__n_estimators': [100, 200],
    'classifier__max_depth': [10, 20, None],
    'classifier__min_samples_split': [2, 5],
    'classifier__min_samples_leaf': [1, 2]
}

grid_search = GridSearchCV(clf, param_grid, cv=5, n_jobs=-1)
grid_search.fit(X_train, y_train)

print("Найкращі параметри:", grid_search.best_params_)
print("Найкраща оцінка крос-валідації:", grid_search.best_score_)

best_model = grid_search.best_estimator_
y_pred = best_model.predict(X_test)

print("\nЗвіт класифікації:\n", classification_report(y_test, y_pred))

cm = confusion_matrix(y_test, y_pred)
plt.figure(figsize=(10,7))
sns.heatmap(cm, annot=True, fmt='d', cmap='Blues')
plt.title('Матриця помилок')
plt.xlabel('Передбачені значення')
plt.ylabel('Справжні значення')
plt.show()

feature_importance = best_model.named_steps['classifier'].feature_importances_
feature_names = (numeric_features +
                 list(best_model.named_steps['preprocessor']
                      .named_transformers_['cat']
                      .named_steps['onehot']
                      .get_feature_names(categorical_features)))

importance_df = pd.DataFrame({'feature': feature_names, 'importance': feature_importance})
importance_df = importance_df.sort_values('importance', ascending=False)

plt.figure(figsize=(10,7))
sns.barplot(x='importance', y='feature', data=importance_df)
plt.title('Важливість ознак')
plt.show()

print("\nВисновки:")
print("1. Модель досягла хорошої точності на тестовому наборі.")
print("2. Найважливішими ознаками для класифікації виявилися: ", ', '.join(importance_df['feature'][:3]))
print("3. Для покращення моделі можна спробувати інші алгоритми, такі як SVM або Gradient Boosting.")
print("4. Збільшення розміру навчального набору також може покращити продуктивність моделі.")