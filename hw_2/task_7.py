from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.orm import sessionmaker
import requests
from bs4 import BeautifulSoup
from sqlalchemy.orm import DeclarativeBase

class Base(DeclarativeBase):
    pass

class Product(Base):
    __tablename__ = 'products'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    price = Column(String)
    link = Column(String)

    def __repr__(self):
        return f"<Product(name='{self.name}', price='{self.price}')>"


engine = create_engine('sqlite:///products.db', echo=True)
Base.metadata.create_all(engine)

Session = sessionmaker(bind=engine)
session = Session()

url = 'https://rozetka.com.ua/ua/tehnika/mobilnye-telefony/apple/c464=apple/'
response = requests.get(url)
soup = BeautifulSoup(response.text, 'html.parser')

products = soup.find_all('div', class_='goods-tile__inner')

for product in products:
    name_tag = product.find('a', class_='goods-tile__heading')
    price_tag = product.find('span', class_='goods-tile__price-value')

    if name_tag and price_tag:
        name = name_tag.text.strip()
        price = price_tag.text.strip()
        link = 'https://rozetka.com.ua' + name_tag['href']

        new_product = Product(
            name=name,
            price=price,
            link=link
        )

        session.add(new_product)

session.commit()

session.close()

print('Дані успішно збережені у базу даних.')