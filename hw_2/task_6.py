import sqlite3
import hashlib
import re


class DatabaseManager:
    def __init__(self, db_name='users.db'):
        self.conn = sqlite3.connect(db_name)
        self.cursor = self.conn.cursor()
        self._create_tables()

    def _create_tables(self):
        self.cursor.execute('''
        CREATE TABLE IF NOT EXISTS users (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            username TEXT UNIQUE NOT NULL,
            password TEXT NOT NULL,
            email TEXT UNIQUE NOT NULL
        )
        ''')

        self.cursor.execute('''
        CREATE TABLE IF NOT EXISTS site_registrations (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            user_id INTEGER,
            site_name TEXT NOT NULL,
            login TEXT NOT NULL,
            password TEXT,
            login_type TEXT DEFAULT 'other',
            FOREIGN KEY(user_id) REFERENCES users(id)
        )
        ''')

        self.conn.commit()


class User:
    def __init__(self, username, password, email, db_manager):
        self.username = username
        self.password = self._hash_password(password)
        self.email = email
        self.db_manager = db_manager

    @staticmethod
    def _hash_password(password):
        return hashlib.sha256(password.encode()).hexdigest()

    def _validate_email(self):
        email_regex = r'^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$'
        return re.match(email_regex, self.email) is not None

    def _check_existing_user(self):
        cursor = self.db_manager.cursor
        cursor.execute(
            'SELECT * FROM users WHERE username = ? OR email = ?',
            (self.username, self.email)
        )
        return cursor.fetchone() is not None

    def register(self):
        try:
            if not self._validate_email():
                print("Некоректний формат email!")
                return None

            if self._check_existing_user():
                print("Користувач з таким іменем або email вже існує!")
                return None

            cursor = self.db_manager.cursor
            cursor.execute(
                'INSERT INTO users (username, password, email) VALUES (?, ?, ?)',
                (self.username, self.password, self.email)
            )
            self.db_manager.conn.commit()

            print(f"Користувач {self.username} успішно зареєстрований!")
            return cursor.lastrowid

        except sqlite3.Error as e:
            print(f"Помилка реєстрації: {e}")
            return None

    @classmethod
    def login(cls, username, password, db_manager):
        hashed_password = cls._hash_password(password)
        cursor = db_manager.cursor

        cursor.execute(
            'SELECT id FROM users WHERE username = ? AND password = ?',
            (username, hashed_password)
        )
        user = cursor.fetchone()

        return user[0] if user else None


class SiteRegistration:
    def __init__(self, db_manager):
        self.db_manager = db_manager

    def add_site_registration(self, user_id, site_name, login,
                              password=None, login_type='other'):
        try:
            cursor = self.db_manager.cursor
            cursor.execute('SELECT * FROM users WHERE id = ?', (user_id,))
            if not cursor.fetchone():
                print("Користувача не знайдено!")
                return None

            cursor.execute(
                'SELECT * FROM site_registrations WHERE user_id = ? AND site_name = ?',
                (user_id, site_name)
            )
            existing_reg = cursor.fetchone()

            if existing_reg:
                cursor.execute('''
                    UPDATE site_registrations 
                    SET login = ?, password = ?, login_type = ?
                    WHERE user_id = ? AND site_name = ?
                ''', (login, password, login_type, user_id, site_name))
            else:
                cursor.execute('''
                    INSERT INTO site_registrations 
                    (user_id, site_name, login, password, login_type) 
                    VALUES (?, ?, ?, ?, ?)
                ''', (user_id, site_name, login, password, login_type))

            self.db_manager.conn.commit()
            print(f"Реєстрація на сайті {site_name} додана/оновлена!")
            return cursor.lastrowid

        except sqlite3.Error as e:
            print(f"Помилка реєстрації на сайті: {e}")
            return None

    def get_user_site_registrations(self, user_id):
        cursor = self.db_manager.cursor
        cursor.execute(
            'SELECT * FROM site_registrations WHERE user_id = ?',
            (user_id,)
        )
        return cursor.fetchall()


def main_menu():
    db_manager = DatabaseManager()
    site_registration = SiteRegistration(db_manager)
    current_user_id = None

    while True:
        print("\n--- Система Реєстрації ---")
        print("1. Реєстрація")
        print("2. Вхід")
        print("3. Додати реєстрацію на сайті")
        print("4. Показати мої реєстрації")
        print("5. Вихід")

        choice = input("Оберіть опцію: ")

        if choice == '1':
            username = input("Введіть логін: ")
            password = input("Введіть пароль: ")
            email = input("Введіть email: ")

            user = User(username, password, email, db_manager)
            current_user_id = user.register()

        elif choice == '2':
            username = input("Введіть логін: ")
            password = input("Введіть пароль: ")

            current_user_id = User.login(username, password, db_manager)
            if current_user_id:
                print("Успішний вхід!")
            else:
                print("Невірний логін або пароль!")

        elif choice == '3':
            if not current_user_id:
                print("Спочатку увійдіть в систему!")
                continue

            site_name = input("Введіть назву сайту: ")
            login = input("Введіть логін: ")
            password = input("Введіть пароль (можна пропустити): ") or None
            login_type = input("Введіть тип входу (google/apple/facebook/other): ")

            site_registration.add_site_registration(
                current_user_id, site_name, login, password, login_type
            )

        elif choice == '4':
            if not current_user_id:
                print("Спочатку увійдіть в систему!")
                continue

            registrations = site_registration.get_user_site_registrations(current_user_id)
            if registrations:
                print("\nВаші реєстрації:")
                for reg in registrations:
                    print(f"Сайт: {reg[2]}, Логін: {reg[3]}, Тип входу: {reg[5]}")
            else:
                print("У вас немає жодної реєстрації.")

        elif choice == '5':
            print("До побачення!")
            break

        else:
            print("Невірний вибір. Спробуйте знову.")


if __name__ == "__main__":
    main_menu()