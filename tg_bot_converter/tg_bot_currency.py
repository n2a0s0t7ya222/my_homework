import telebot
import requests
import json
import os

from telebot import types
from dotenv import find_dotenv
from dotenv import load_dotenv

env_file = find_dotenv(".env")
load_dotenv(env_file)

API_TOKEN = os.environ.get("TBOT_TOKEN")
bot = telebot.TeleBot(API_TOKEN)

history_file = 'conversion_history.json'


def save_history(converted_data):
    if os.path.exists(history_file):
        with open(history_file, 'r') as file:
            data = json.load(file)
    else:
        data = []

    data.append(converted_data)

    if len(data) > 10:
        data.pop(0)

    with open(history_file, 'w') as file:
        json.dump(data, file, indent=4)


class CurrencyRate:
    def __init__(self):
        self.rate_data = self._get_rates()

    def _get_rates(self):
        response = requests.get('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json')
        if response.status_code == 200:
            return response.json()
        return []

    def get_rate(self, currency):
        for rate in self.rate_data:
            if rate['cc'] == currency:
                return rate['rate']
        return None

    def convert_currency(self, amount, into_currency, from_to_currency):
        if from_to_currency == 'UAH':
            rate_to = self.get_rate(into_currency)
            if rate_to:
                return amount / rate_to
            elif into_currency == 'UAH':
                rate_from = self.get_rate(from_to_currency)
                if rate_from:
                    return amount * rate_from

        else:
            rate_from = self.get_rate(from_to_currency)
            rate_to = self.get_rate(into_currency)
            if rate_from and rate_to:
                return amount * (rate_from / rate_to)
        return None


rate_of_currency = CurrencyRate()


@bot.message_handler(commands=['start', 'hello'])
def send_welcome(message):
    if message.text == '/hello':
        bot.reply_to(message, 'Вітаю вас!')
    elif message.text == '/start':
        bot.reply_to(message,
                     'Вітаю! Я чат бот для конвертаціїї валют, '
                     'оберіть опціїї  які будуть вказані нижче. '
                     'Якщо потрібна допомога, то оберіть опцію: '
                     '"Запросити допомогу"')
        main_menu(message)
def main_menu(message):
    keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.row('Конвертування валюти', 'Запросити допомогу')
    bot.send_message(message.chat.id, 'Оберіть опцію:', reply_markup=keyboard)


@bot.message_handler(func=lambda message: message.text == 'Запросити допомогу')
def get_help(message):
    bot.reply_to(message,
                 'Спочатку введіть вихідну валюту, потім оберіть валюту для конвертації, '
                 'після чого введіть потрібну суму. Наприклад: '
                 '"Оберіть вихідну валюту: UAH" \n'
                 '"Оберіть валюту для конвертації: USD" \n'
                 '"Введіть суму для конвертації: 100" ')


@bot.message_handler(func=lambda message: message.text == 'Конвертування валюти')
def convert_into(message):
    markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    button_1 = telebot.types.KeyboardButton('USD')
    button_2 = telebot.types.KeyboardButton('EUR')
    button_3 = telebot.types.KeyboardButton('UAH')
    button_4 = telebot.types.KeyboardButton('PLN')
    markup.add(button_1, button_2, button_3, button_4)
    your_message = bot.send_message(message.chat.id, "Оберіть вихідну валюту:", reply_markup=markup)
    bot.register_next_step_handler(your_message, convert_from_to)

def convert_from_to(message):
    base_currency = message.text.upper()
    markup = telebot.types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    button_1 = telebot.types.KeyboardButton('USD')
    button_2 = telebot.types.KeyboardButton('EUR')
    button_3 = telebot.types.KeyboardButton('UAH')
    button_4 = telebot.types.KeyboardButton('PLN')
    markup.add(button_1, button_2, button_3, button_4)
    your_message = bot.send_message(message.chat.id, f"Оберіть валюту для конвертації {base_currency}:", reply_markup=markup)
    bot.register_next_step_handler(your_message, lambda m: process_currency(m, base_currency))

def process_currency(message, base_currency):
    target_currency = message.text.upper()
    your_message = bot.send_message(message.chat.id, f'Введіть суму '
                                                      f'для конвертації з {target_currency} '
                                                      f'в {base_currency}:')
    bot.register_next_step_handler(your_message, lambda m: convert_currency(m, base_currency, target_currency))

def convert_currency(message, base_currency, target_currency):
    amount = float(message.text)
    converted_amount = rate_of_currency.convert_currency(amount, base_currency, target_currency)
    bot.send_message(message.chat.id, f'{amount} {target_currency} = {converted_amount:.2f} {base_currency}')
    converted_data = {
        'amount': amount, 'base_currency': base_currency,
        'target_currency': target_currency, 'converted_amount': converted_amount
    }
    save_history(converted_data)


@bot.message_handler(func=lambda message: True)
def currency_handler(message):
    parts = message.text.split()
    amount = float(parts[0])
    currency_from = parts[1].upper()
    keyboard = telebot.types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    keyboard.row('USD', 'EUR', 'UAH', 'PLN')
    bot.send_message(message.chat.id,'Оберіть валюту для конвертації: ', reply_markup=keyboard)
    bot.register_next_step_handler(message, target_handler, amount, currency_from)
    main_menu(message)

def target_handler(message, amount, currency_from):
    to_currency = message.text.upper()
    converted_amount = rate_of_currency.convert_currency(amount, currency_from, to_currency)
    bot.send_message(message.chat.id, f'{amount} {to_currency} = {converted_amount:.2f} {currency_from}')
    converted_data = {
        'amount': amount, 'base_currency': currency_from,
        'target_currency': to_currency, 'converted_amount': converted_amount
    }
    save_history(converted_data)

bot.polling()
