import unittest
import requests
from unittest.mock import patch


class WeatherService:
    def __init__(self, api_key):
        self.api_key = api_key
        self.base_url = "http://api.openweathermap.org/data/2.5/weather"

    def get_weather(self, city):
        params = {
            'q': city,
            'appid': self.api_key,
            'units': 'metric'
        }
        response = requests.get(self.base_url, params=params)
        if response.status_code == 200:
            return response.json()
        else:
            raise Exception(f"Error fetching weather data: {response.status_code}")


class TestWeatherService(unittest.TestCase):
    def setUp(self):
        self.api_key = "8afc2f7ac0683fb7c9c7beea549f8b93"
        self.weather_service = WeatherService(self.api_key)

    def test_successful_weather_request(self):
        mock_response = {
            "main": {
                "temp": 20.5,
                "humidity": 65
            },
            "weather": [{
                "description": "clear sky"
            }]
        }

        with patch('requests.get') as mock_get:
            mock_get.return_value.status_code = 200
            mock_get.return_value.json.return_value = mock_response

            result = self.weather_service.get_weather("Kyiv")

            self.assertEqual(result["main"]["temp"], 20.5)
            self.assertEqual(result["main"]["humidity"], 65)
            self.assertEqual(result["weather"][0]["description"], "clear sky")

    def test_failed_weather_request(self):
        with patch('requests.get') as mock_get:
            mock_get.return_value.status_code = 404

            with self.assertRaises(Exception):
                self.weather_service.get_weather("NonExistentCity")

    def test_api_parameters(self):
        with patch('requests.get') as mock_get:
            mock_get.return_value.status_code = 200
            mock_get.return_value.json.return_value = {}

            self.weather_service.get_weather("Kyiv")

            mock_get.assert_called_once()
            args, kwargs = mock_get.call_args

            self.assertIn('params', kwargs)
            params = kwargs['params']
            self.assertEqual(params['q'], "Kyiv")
            self.assertEqual(params['appid'], self.api_key)
            self.assertEqual(params['units'], 'metric')


if __name__ == '__main__':
    unittest.main()
