import threading
import time

def is_prime(n):
    if n <= 1:
        return False
    if n == 2:
        return True
    if n % 2 == 0:
        return False
    max_divisor = int(n ** 0.5) + 1
    for d in range(3, max_divisor, 2):
        if n % d == 0:
            return False
    return True


def find_primes_single_thread(start, end):
    primes = []
    for n in range(start, end + 1):
        if is_prime(n):
            primes.append(n)
    return primes


def find_primes_multi_thread(start, end):
    mid = (start + end) // 2
    result = []

    def find_primes(start, end, result):
        primes = []
        for n in range(start, end + 1):
            if is_prime(n):
                primes.append(n)
        result.extend(primes)

    t1 = threading.Thread(target=find_primes, args=(start, mid, result))
    t2 = threading.Thread(target=find_primes, args=(mid + 1, end, result))

    t1.start()
    t2.start()

    t1.join()
    t2.join()

    return result


start = 10
end = 100
print("Один потік:", find_primes_single_thread(start, end))
print("Багатопоточність:", find_primes_multi_thread(start, end))

start_time = time.time()
find_primes_single_thread(10, 1000)
end_time = time.time()
print("Час виконання одного потоку:", end_time - start_time)

start_time = time.time()
find_primes_multi_thread(10, 1000)
end_time = time.time()
print("Час виконання багатопоточності:", end_time - start_time)