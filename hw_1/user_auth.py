import sqlite3
from hashlib import sha256
import re


class User:
    def __init__(self, username, password, email):
        self.username = username
        self.password = sha256(password.encode()).hexdigest()
        self.email = email
        self.db_name = 'users.db'

        self._create_table()

    def _create_table(self):
        conn = sqlite3.connect(self.db_name)
        cursor = conn.cursor()

        cursor.execute('''
            CREATE TABLE IF NOT EXISTS users (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                username TEXT UNIQUE,
                password TEXT,
                email TEXT UNIQUE
            )
        ''')

        conn.commit()
        conn.close()

    def _validate_email(self, email):
        pattern = r'^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$'
        return re.match(pattern, email) is not None

    def _validate_username(self, username):
        return len(username) >= 3 and username.isalnum()

    def register(self):
        if not self._validate_email(self.email):
            return False, "Некоректний формат email"

        if not self._validate_username(self.username):
            return False, "Ім'я користувача має містити мінімум 3 символи та складатися з букв і цифр"

        conn = sqlite3.connect(self.db_name)
        cursor = conn.cursor()

        try:
            cursor.execute('''
                INSERT INTO users (username, password, email)
                VALUES (?, ?, ?)
            ''', (self.username, self.password, self.email))

            conn.commit()
            return True, "Реєстрація успішна!"

        except sqlite3.IntegrityError as e:
            if "username" in str(e):
                return False, "Користувач з таким іменем вже існує"
            elif "email" in str(e):
                return False, "Цей email вже зареєстрований"
            return False, "Помилка при реєстрації"

        finally:
            conn.close()

    @staticmethod
    def login(username, password):
        hashed_password = sha256(password.encode()).hexdigest()

        conn = sqlite3.connect('users.db')
        cursor = conn.cursor()

        cursor.execute('''
            SELECT * FROM users 
            WHERE username = ? AND password = ?
        ''', (username, hashed_password))

        user = cursor.fetchone()
        conn.close()

        return user is not None


def main():
    while True:
        print("\n=== Система аутентифікації ===")
        print("1. Зареєструватися")
        print("2. Увійти")
        print("3. Вийти")

        choice = input("\nОберіть опцію (1-3): ")

        if choice == "1":
            username = input("Введіть ім'я користувача: ")
            password = input("Введіть пароль: ")
            email = input("Введіть email: ")

            user = User(username, password, email)
            success, message = user.register()
            print(f"\n{message}")

        elif choice == "2":
            username = input("Введіть ім'я користувача: ")
            password = input("Введіть пароль: ")

            if User.login(username, password):
                print("\nУспішний вхід!")
            else:
                print("\nНеправильні дані!")

        elif choice == "3":
            print("\nДо побачення!")
            break

        else:
            print("\nНеправильний вибір. Спробуйте ще раз.")


if __name__ == "__main__":
    main()