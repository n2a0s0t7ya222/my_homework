import sys
from functools import wraps


def check_division_error(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except ZeroDivisionError:
            print(f"Помилка: Спроба ділення на нуль у функції {func.__name__}")
            sys.exit(1)

    return wrapper


def check_index_error(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except IndexError:
            print(f"Помилка: Вихід за межі списку у функції {func.__name__}")
            sys.exit(1)

    return wrapper


@check_division_error
def divide(a, b):
    return a / b


@check_index_error
def get_element(lst, idx):
    return lst[idx]


def run_tests():
    print("Запуск тестів...")

    print("\nТест 1: Коректне ділення")
    try:
        result = divide(10, 2)
        print(f"Результат: {result}")
    except SystemExit:
        print("Тест 1 завершився помилкою")

    print("\nТест 2: Ділення на нуль")
    try:
        result = divide(10, 0)
        print(f"Результат: {result}")
    except SystemExit:
        print("Тест 2 завершився помилкою")

    print("\nТест 3: Коректне отримання елементу")
    test_list = [1, 2, 3, 4, 5]
    try:
        result = get_element(test_list, 2)
        print(f"Результат: {result}")
    except SystemExit:
        print("Тест 3 завершився помилкою")

    print("\nТест 4: Некоректний індекс")
    try:
        result = get_element(test_list, 10)
        print(f"Результат: {result}")
    except SystemExit:
        print("Тест 4 завершився помилкою")


if __name__ == "__main__":
    run_tests()