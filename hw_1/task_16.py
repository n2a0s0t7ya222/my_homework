import mysql.connector
from mysql.connector import Error
from hashlib import sha256
import re
from typing import Optional, Tuple, List
import getpass
from datetime import datetime


class DatabaseConnection:
    def __init__(self):
        self.config = {
            'host': 'localhost',
            'user': 'your_username',
            'password': 'your_password',
            'database': 'auth_system'
        }

    def __enter__(self):
        self.connection = mysql.connector.connect(**self.config)
        return self.connection

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.connection.is_connected():
            self.connection.close()


class DatabaseInitializer:
    @staticmethod
    def initialize_database():
        with DatabaseConnection() as connection:
            cursor = connection.cursor()

            # Створення таблиці користувачів
            cursor.execute('''
                CREATE TABLE IF NOT EXISTS users (
                    id INT AUTO_INCREMENT PRIMARY KEY,
                    username VARCHAR(50) UNIQUE,
                    password VARCHAR(256),
                    email VARCHAR(100) UNIQUE,
                    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                    CONSTRAINT uc_username UNIQUE (username),
                    CONSTRAINT uc_email UNIQUE (email)
                )
            ''')

            # Створення таблиці веб-сайтів
            cursor.execute('''
                CREATE TABLE IF NOT EXISTS websites (
                    id INT AUTO_INCREMENT PRIMARY KEY,
                    user_id INT,
                    site_name VARCHAR(100),
                    login_username VARCHAR(100),
                    password VARCHAR(256),
                    login_type ENUM('direct', 'google', 'apple', 'facebook'),
                    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
                    CONSTRAINT uc_user_site UNIQUE (user_id, site_name)
                )
            ''')

            connection.commit()


class User:
    def __init__(self, username: str, password: str, email: str):
        self.username = username
        self.password = sha256(password.encode()).hexdigest()
        self.email = email
        self._id = None

    @property
    def id(self) -> Optional[int]:
        return self._id

    def _validate_email(self) -> bool:
        pattern = r'^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$'
        return bool(re.match(pattern, self.email))

    def _validate_username(self) -> bool:
        return len(self.username) >= 3 and self.username.isalnum()

    def register(self) -> Tuple[bool, str]:
        if not self._validate_email():
            return False, "Некоректний формат email"

        if not self._validate_username():
            return False, "Ім'я користувача має містити мінімум 3 символи та складатися з букв і цифр"

        try:
            with DatabaseConnection() as connection:
                cursor = connection.cursor()

                cursor.execute('''
                    INSERT INTO users (username, password, email)
                    VALUES (%s, %s, %s)
                ''', (self.username, self.password, self.email))

                self._id = cursor.lastrowid
                connection.commit()
                return True, "Реєстрація успішна!"

        except Error as e:
            if "uc_username" in str(e):
                return False, "Користувач з таким іменем вже існує"
            elif "uc_email" in str(e):
                return False, "Цей email вже зареєстрований"
            return False, f"Помилка при реєстрації: {str(e)}"

    @staticmethod
    def login(username: str, password: str) -> Optional[int]:
        hashed_password = sha256(password.encode()).hexdigest()

        with DatabaseConnection() as connection:
            cursor = connection.cursor()
            cursor.execute('''
                SELECT id FROM users 
                WHERE username = %s AND password = %s
            ''', (username, hashed_password))

            result = cursor.fetchone()
            return result[0] if result else None


class WebsiteManager:
    def __init__(self, user_id: int):
        self.user_id = user_id

    def add_website(self, site_name: str, login_type: str) -> Tuple[bool, str]:
        try:
            with DatabaseConnection() as connection:
                cursor = connection.cursor()

                # Перевірка чи вже є реєстрація на цьому сайті
                cursor.execute('''
                    SELECT id, login_type FROM websites
                    WHERE user_id = %s AND site_name = %s
                ''', (self.user_id, site_name))

                existing = cursor.fetchone()
                if existing and existing[1] == login_type:
                    return False, "Ви вже зареєстровані на цьому сайті з таким типом входу"

                login_username = None
                password = None

                if login_type == 'direct':
                    login_username = input("Введіть логін для сайту: ")
                    password = getpass.getpass("Введіть пароль для сайту: ")
                    password = sha256(password.encode()).hexdigest()

                cursor.execute('''
                    INSERT INTO websites (user_id, site_name, login_username, password, login_type)
                    VALUES (%s, %s, %s, %s, %s)
                ''', (self.user_id, site_name, login_username, password, login_type))

                connection.commit()
                return True, "Сайт успішно додано!"

        except Error as e:
            return False, f"Помилка при додаванні сайту: {str(e)}"

    def get_websites(self) -> List[tuple]:
        with DatabaseConnection() as connection:
            cursor = connection.cursor(dictionary=True)
            cursor.execute('''
                SELECT site_name, login_username, login_type, created_at
                FROM websites
                WHERE user_id = %s
                ORDER BY created_at DESC
            ''', (self.user_id,))

            return cursor.fetchall()


def main():
    DatabaseInitializer.initialize_database()
    current_user_id = None

    while True:
        if current_user_id is None:
            print("\n=== Система аутентифікації ===")
            print("1. Зареєструватися")
            print("2. Увійти")
            print("3. Вийти")

            choice = input("\nОберіть опцію (1-3): ")

            if choice == "1":
                username = input("Введіть ім'я користувача: ")
                password = getpass.getpass("Введіть пароль: ")
                email = input("Введіть email: ")

                user = User(username, password, email)
                success, message = user.register()
                print(f"\n{message}")

            elif choice == "2":
                username = input("Введіть ім'я користувача: ")
                password = getpass.getpass("Введіть пароль: ")

                user_id = User.login(username, password)
                if user_id:
                    print("\nУспішний вхід!")
                    current_user_id = user_id
                else:
                    print("\nНеправильні дані!")

            elif choice == "3":
                print("\nДо побачення!")
                break
        else:
            print("\n=== Управління сайтами ===")
            print("1. Додати новий сайт")
            print("2. Переглянути мої сайти")
            print("3. Вийти з аккаунту")

            choice = input("\nОберіть опцію (1-3): ")

            website_manager = WebsiteManager(current_user_id)

            if choice == "1":
                site_name = input("Введіть назву сайту: ")
                print("\nТипи входу:")
                print("1. Прямий вхід")
                print("2. Google")
                print("3. Apple")
                print("4. Facebook")

                login_type_choice = input("Оберіть тип входу (1-4): ")
                login_types = {
                    "1": "direct",
                    "2": "google",
                    "3": "apple",
                    "4": "facebook"
                }

                if login_type_choice in login_types:
                    success, message = website_manager.add_website(
                        site_name,
                        login_types[login_type_choice]
                    )
                    print(f"\n{message}")
                else:
                    print("\nНеправильний вибір типу входу!")

            elif choice == "2":
                websites = website_manager.get_websites()
                if websites:
                    print("\nВаші сайти:")
                    for site in websites:
                        print(f"\nСайт: {site['site_name']}")
                        print(f"Тип входу: {site['login_type']}")
                        if site['login_type'] == 'direct':
                            print(f"Логін: {site['login_username']}")
                        print(f"Дата додавання: {site['created_at']}")
                else:
                    print("\nУ вас ще немає збережених сайтів")

            elif choice == "3":
                current_user_id = None
                print("\nВи вийшли з аккаунту")


if __name__ == "__main__":
    main()