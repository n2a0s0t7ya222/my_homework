input_data = [10, 100]

for data_size in input_data:
    result_data = []
    result_data_2 = []
    result_data_3 = []
    my_data = list(range(2, data_size + 1))
    my_data_2 = list(range(3, data_size + 1))
    my_data_3 = list(range(5, data_size + 1))

    for data_item in my_data:
        data = data_item % 2 != 0
        if data:
            result_data.append(data_item)
    print(f"First data {result_data}")

    for data_item_2 in my_data_2:
        data_2 = data_item_2 % 3 != 0
        if data_2:
            result_data_2.append(data_item_2)
    print(f"Second data {result_data_2}")

    for data_item_3 in my_data_3:
        data_3 = data_item_3 % 5 != 0
        if data_3:
            result_data_3.append(data_item_3)
    print(f"Third data {result_data_3}")

