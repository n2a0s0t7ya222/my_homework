first_number = int(input("Enter first number: "))
second_number = int(input("Enter second number: "))

if first_number > second_number:
    print(f"{first_number} is greater than {second_number}")

else:
    print(f"{second_number} is greater than {first_number}")