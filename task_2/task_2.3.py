first_number = int(input("Enter first number: "))
second_number = int(input("Enter second number: "))
third_number = int(input("Enter third number: "))

biggest = max(first_number, second_number, third_number)
print(f'Biggest number {biggest}')

smallest = min(first_number, second_number, third_number)
print(f'Smallest number {smallest}')

middle = int((first_number+second_number+third_number)/3)
print(f'Middle number {middle}')

