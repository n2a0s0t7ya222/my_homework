input_number = int(input("Enter a number between 1 and 100: "))

acceptable_values = list(range(1, 101))
acceptable_number = input_number in acceptable_values
fizz_buzz = input_number % 3 == 0 and input_number % 5 == 0

if acceptable_number and fizz_buzz:
    print('fizz buzz')

elif acceptable_number and input_number % 5 == 0:
    print('buzz')

elif acceptable_number and input_number % 3 == 0:
    print('fizz')

else:
    print(input_number)

