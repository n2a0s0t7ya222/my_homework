from pprint import pprint

def orders_file(read_file):
    with open (read_file, 'r') as file:
        text = file.read()
        replaced_text = text.replace("\n\n", "@@@")
        content = replaced_text.split("@@@")

    count_dict = {}

    for content_name in content:

        if content_name in count_dict:
            count_dict[content_name] += 1

        else:
            count_dict[content_name] = 1

    pprint(count_dict)

orders_file("orders (1).txt")