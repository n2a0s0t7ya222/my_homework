import requests

API_KEY = "e90fab7014064d2c88795d9fd95afa6f"

class OpenWeatherMap:
    def __init__(self, city):
        self.city = city
        self.base_url = "http://api.openweathermap.org/data/2.5/weather?q={0}&appid={1}".format(city, API_KEY)
        response = requests.get(self.base_url)
        self.data = response.json()

    def get_temp(self):
        return self.data['main']['temp']

    def get_weather(self):
        return self.data['weather'][0]['description']

    def get_wind(self):
        return self.data['wind']['speed']

    def get_city(self):
        return self.city

    def get_text(self):
        return str(self.data)

    def get_any_key(self, *keys):
        return {key: self.data.get(key) for key in keys}

    def __str__(self):
        return "Weather information for {}: {}".format(self.city, self.get_text())

def main():
    city = input("Введіть назву міста: ")
    weather_info = OpenWeatherMap(city)
    print(weather_info)

    keys = input("Введіть ключі для виводу: ").split()
    info = weather_info.get_any_key(*keys)
    print(info)

if __name__ == "__main__":
    main()
