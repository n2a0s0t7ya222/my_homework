import os

# import telebot
from dotenv import find_dotenv
from dotenv import load_dotenv
#
# env_file = find_dotenv(".env")
# load_dotenv(env_file)
#
# TBOT_TOKEN = os.environ.get("TBOT_TOKEN")
# bot = telebot.TeleBot(TBOT_TOKEN)
#
# @bot.message_handler(func=lambda message: True)
# def handle_message(message: telebot.types.Message):
#     bot.reply_to(message, "Ваше повідомлення: " + message.text)
#
# bot.polling()
#
import telebot
import requests
import json
from telebot import types

env_file = find_dotenv(".env")
load_dotenv(env_file)

API_TOKEN = os.environ.get("TBOT_TOKEN")
bot = telebot.TeleBot(API_TOKEN)

# Зберігаємо останні 10 запитів
history_file = 'conversion_history.json'


def load_history():
    try:
        with open(history_file, 'r') as f:
            return json.load(f)
    except FileNotFoundError:
        return []


def save_history(history):
    with open(history_file, 'w') as f:
        json.dump(history, f)


def get_currency_rates():
    response = requests.get('https://api.monobank.ua/bank/currency')
    return response.json()


@bot.message_handler(commands=['start'])
def send_welcome(message):
    bot.reply_to(message, "Привіт! Я ваш валютний конвертер. Введіть суму та виберіть валюту для конвертації.")
    show_currency_buttons(message)


def show_currency_buttons(message):
    markup = types.ReplyKeyboardMarkup(one_time_keyboard=True)
    currencies = ['USD', 'EUR', 'UAH', 'GBP']
    for currency in currencies:
        markup.add(currency)
    bot.send_message(message.chat.id, "Оберіть валюту для конвертації:", reply_markup=markup)


@bot.message_handler(func=lambda message: message.text in ['USD', 'EUR', 'UAH', 'GBP'])
def handle_currency_selection(message):
    bot.send_message(message.chat.id, "Введіть суму для конвертації:")
    bot.register_next_step_handler(message, process_amount, message.text)


def process_amount(message, currency_to):
    try:
        amount = float(message.text)
        currency_from = 'UAH'  # Вихідна валюта
        rates = get_currency_rates()

        # Знаходимо курс
        rate = next(
            item for item in rates if item['currencyCodeA'] == currency_to and item['currencyCodeB'] == currency_from)
        converted_amount = amount / rate['rateBuy']

        # Зберігаємо в історії
        history = load_history()
        history.append({'from': currency_from, 'to': currency_to, 'amount': amount, 'converted': converted_amount})
        if len(history) > 10:
            history.pop(0)
        save_history(history)

        bot.send_message(message.chat.id, f"{amount} {currency_from} = {converted_amount:.2f} {currency_to}")
    except ValueError:
        bot.send_message(message.chat.id, "Будь ласка, введіть коректну суму.")
    except StopIteration:
        bot.send_message(message.chat.id, "Не вдалося знайти курс для конвертації.")
    except Exception as e:
        bot.send_message(message.chat.id, f"Сталася помилка: {str(e)}")


@bot.message_handler(commands=['history'])
def show_history(message):
    history = load_history()
    if history:
        history_message = "Останні 10 запитів на конвертацію:\n"
        for entry in history:
            history_message += f"{entry['amount']} {entry['from']} -> {entry['converted']:.2f} {entry['to']}\n"
        bot.send_message(message.chat.id, history_message)
    else:
        bot.send_message(message.chat.id, "Історія порожня.")


bot.polling()
