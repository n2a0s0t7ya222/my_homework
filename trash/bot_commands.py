import os

import telebot
from dotenv import find_dotenv
from dotenv import load_dotenv

env_file = find_dotenv(".env")
load_dotenv(env_file)

TBOT_TOKEN = os.environ.get("TBOT_TOKEN")
bot = telebot.TeleBot(TBOT_TOKEN)


@bot.message_handler(commands=['start'])
def start(message):
    keyboard = telebot.types.ReplyKeyboardMarkup()
    keyboard.row('Button 1', 'Button 2')
    keyboard.row('Button 3', 'Button 4')
    bot.send_message(message.chat.id, 'Pick the button:', reply_markup=keyboard)

@bot.message_handler(func=lambda message: True)
def handle_button_click(message):
    if message.text == 'Button 1':
        bot.send_message(message.chat.id, 'u pressed 1')
    elif message.text == 'Button 2':
        bot.send_message(message.chat.id, '2')
    elif message.text == 'Button 3':
        bot.send_message(message.chat.id, '3')
    elif message.text == 'Button 4':
        bot.send_message(message.chat.id, '4')

    # keyboard = telebot.types.InlineKeyboardMarkup()
    # button_1 = telebot.types.InlineKeyboardButton('Button 1', callback_data='btn1')
    # button_2 = telebot.types.InlineKeyboardButton('Button 2', callback_data='btn2')
    # keyboard.add(button_1, button_2)
    # bot.send_message(message.chat.id, 'Виберіть кнопку: ', reply_markup=keyboard)
    # if message.text == '/start':
    #     bot.reply_to(message, "Привіт!")
    # elif message.text == '/help':
    #     bot.reply_to(message, 'допомога')

bot.polling()

#         amount = float(message.text)
#         currency_from = 'UAH'  # Вихідна валюта
#         currency_to = message.reply_to_message.text  # Цільова валюта
#         rates = get_currency_rates()
#
#         # Знаходимо курс
#         rate = next(
#             item for item in rates if item['currencyCodeA'] == currency_to and item['currencyCodeB'] == currency_from)
#         converted_amount = amount / rate['rateBuy']
#
        # Зберігаємо в

    #     bot.send_message(message.chat.id, f"{amount} {currency_from} = {converted_amount:.2f} {currency_to}")
    # except ValueError:
    #     bot.send_message(message.chat.id, "Будь ласка, введіть коректну суму.")
    # except StopIteration:
    #     bot.send_message(message.chat.id, "Не вдалося знайти курс для конвертації.")