from flask import Flask, request, jsonify

app = Flask(__name__)

questions_db = [
    {"question": "Скільки буде 2 + 2?", "answer": "4"},
    {"question": "Яка столиця України?", "answer": "Київ"},
]

@app.route('/questions', methods=['GET'])
def get_question():
    return jsonify(questions_db[0])

@app.route('/answer', methods=['POST'])
def check_answer():
    data = request.get_json()
    user_answer = data.get('answer')

    if user_answer == questions_db[0]['answer']:
        result = {"result": "Правильно!"}
    else:
        result = {"result": "Неправильно!"}

    return jsonify(result)

@app.errorhandler(404)
def not_found(e):
    return jsonify({"error": "URL не знайдено"}), 404

if __name__ == '__main__':
    app.run(debug=True)

