import numpy as np

chessboard = np.zeros((8, 8))

chessboard[1::2, ::2] = 1
chessboard[::2, 1::2] = 1

print("3-й рядок:")
print(chessboard[2])

print("\n5-й стовпець:")
print(chessboard[:, 4])

print("\nПідмасив 3x3 в лівому верхньому куті:")
print(chessboard[:3, :3])
