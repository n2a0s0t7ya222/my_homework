import numpy as np

array1 = np.random.rand(5, 5)
array2 = np.random.rand(5, 5)

sum_array1 = np.sum(array1)
sum_array2 = np.sum(array2)
print("Сума елементів першого масиву:", sum_array1)
print("Сума елементів другого масиву:", sum_array2)

diff_arrays = array1 - array2
print("Різниця між елементами двох масивів:")
print(diff_arrays)

mult_arrays = array1 * array2
print("Перемноження елементів двох масивів:")
print(mult_arrays)

pow_arrays = np.power(array1, array2)
print("Підняття кожного елемента першого масиву до ступеня відповідного елемента другого масиву:")
print(pow_arrays)
