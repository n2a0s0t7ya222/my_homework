from PIL import Image
import numpy as np

image_path = "img.png"
image = Image.open(image_path)

image_array = np.array(image)

print("Розмір зображення:", image_array.shape)
print("Тип даних зображення:", image_array.dtype)

for i in range(3):
    channel_values = image_array[:, :, i]
    print(f"Канал {i}:")
    print("Середнє значення:", np.mean(channel_values))
    print("Мінімум:", np.min(channel_values))
    print("Максимум:", np.max(channel_values))

total_intensity = np.sum(image_array)
print("Загальна сума інтенсивності пікселів:", total_intensity)

max_value = np.max(image_array)
normalized_image = image_array / max_value

print("Нормалізоване зображення:")
print(normalized_image)
