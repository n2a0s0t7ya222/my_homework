import pandas as pd

url = "https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data"
column_names = ["sepal_length", "sepal_width", "petal_length", "petal_width", "class"]

iris_df = pd.read_csv(url, header=None, names=column_names)

versicolor_df = iris_df[iris_df['class'] == 'Iris-versicolor']

filtered_versicolor_df = versicolor_df[versicolor_df['petal_length'] > 5.0]

print(filtered_versicolor_df)