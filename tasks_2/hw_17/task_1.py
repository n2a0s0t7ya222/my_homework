import pandas as pd
import matplotlib.pyplot as plt

url = "https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data"
column_names = ["sepal_length", "sepal_width", "petal_length", "petal_width", "class"]

iris_df = pd.read_csv(url, header=None, names=column_names)

mean_sepal_length = iris_df.groupby('class')['sepal_length'].mean()
print("Середня довжина чашелистика для кожного виду ірису:")
print(mean_sepal_length)

max_petal_width_setosa = iris_df[iris_df['class'] == 'Iris-setosa']['petal_width'].max()
print("\nМаксимальна ширина листка для виду 'setosa':", max_petal_width_setosa)

iris_df['petal_length'].hist()
plt.xlabel('Довжина листка')
plt.ylabel('Кількість')
plt.title('Розподіленість довжини листка для всіх ірисів')
plt.show()
