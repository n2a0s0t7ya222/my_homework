import pandas as pd

url = "https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data"
column_names = ["sepal_length", "sepal_width", "petal_length", "petal_width", "class"]

iris_df = pd.read_csv(url, header=None, names=column_names)

mean_petal_width = iris_df.groupby('class')['petal_width'].mean()
print("Середня ширина листка для кожного виду ірису:")
print(mean_petal_width)

min_sepal_length = iris_df.groupby('class')['sepal_length'].min()
print("\nМінімальна довжина чашелистика для кожного виду ірису:")
print(min_sepal_length)

avg_petal_length = iris_df['petal_length'].mean()
iris_with_longer_petal = iris_df[iris_df['petal_length'] > avg_petal_length].groupby('class')['petal_length'].count()
print("\nКількість ірисів кожного виду з довжиною листка більше за середню довжину листка всіх ірисів:")
print(iris_with_longer_petal)
а