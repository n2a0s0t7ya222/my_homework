import seaborn as sns
import matplotlib.pyplot as plt

penguins = sns.load_dataset('penguins')

print(penguins.head())

plt.figure(figsize=(12, 6))
sns.boxplot(data=penguins, x='species', y='flipper_length_mm', palette='deep')
plt.title('Розміри крил у залежності від виду птаха')
plt.xlabel('Вид птаха')
plt.ylabel('Довжина крила (мм)')
plt.show()