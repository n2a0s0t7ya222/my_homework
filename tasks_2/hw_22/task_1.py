import seaborn as sns
import matplotlib.pyplot as plt

penguins = sns.load_dataset('penguins')

print(penguins.head())

plt.figure(figsize=(12, 6))
sns.scatterplot(data=penguins, x='body_mass_g', y='flipper_length_mm', hue='species', style='species', palette='deep')
plt.title('Розподіл ваги та висоти птахів (пінгвінів)')
plt.xlabel('Вага (г)')
plt.ylabel('Довжина крила (мм)')
plt.legend(title='Вид птаха')
plt.show()