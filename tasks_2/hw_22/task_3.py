import seaborn as sns
import matplotlib.pyplot as plt

penguins = sns.load_dataset('penguins')

print(penguins.head())

plt.figure(figsize=(10, 8))
correlation_matrix = penguins.corr()
sns.heatmap(correlation_matrix, annot=True, fmt=".2f", cmap="coolwarm", square=True, cbar_kws={"shrink": .8})
plt.title('Теплова карта кореляцій між ознаками')
plt.show()